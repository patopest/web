import requests
import json
import datetime

from flask import session

from app.config import settings

class Api():

	base_url = settings.get("API_URL")
	session = requests.Session()

	def login(self, body):
		r = self.session.post(self.base_url + '/login', data=json.dumps(body))
		if r.status_code == 202:
			resp = r.json()
			headers = {
				"Authorization": resp['token'],
				#"User-ID": resp['id']
			}
			self.session.headers.update(headers)
			session.clear()
			session['token'] = resp['token']
			session['user_id'] = resp['id']
			session['last_login'] = datetime.datetime.utcnow().timestamp()
			return r
		else:
			return r

	def register(self, body):
		r = self.session.post(self.base_url + '/register', data=json.dumps(body))
		if r.status_code == 201:
			resp = r.json()
			headers = {
				"Authorization": resp['token'],
				#"User-ID": resp['id']
			}
			self.session.headers.update(headers)
			session.clear()
			session['token'] = resp['token']
			session['user_id'] = resp['id']
			session['last_login'] = datetime.datetime.utcnow().timestamp()
			return r
		else:
			return r


	def get(self, url, body=None):
		self.load_session()
		r = self.session.get(self.base_url + url)
		return r

	def post(self, url, body):
		self.load_session()
		r = self.session.post(self.base_url + url, data=json.dumps(body))
		return r

	def put(self, url, body):
		self.load_session()
		r = self.session.put(self.base_url + url, data=json.dumps(body))
		return r

	def delete(self, url):
		self.load_session()
		r = self.session.delete(self.base_url + url)
		return r

	def load_session(self):
		if self.session.headers.get('Authorization'):
			return

		if session.get('user_id') and session.get('token'):
			headers = {
				"Authorization": session.get('token'),
				#"User-ID": session.get('user_id')
			}
			self.session.headers.update(headers)








