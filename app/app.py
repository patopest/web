import logging

from flask import Flask
from flask_bootstrap import Bootstrap
from vyper import v

from app.config import parser, settings
from app.util.vyper import setup_vyper
from app.util.logging import setup_logging

logger = logging.getLogger(__name__)

def configure(**overrides):
	logging.getLogger("requests").setLevel(level=logging.INFO)
	logging.getLogger("urllib3").setLevel(level=logging.INFO)
	logging.getLogger('vyper').setLevel(level=logging.WARNING)
	logging.getLogger('werkzeug').setLevel(level=logging.INFO)
	#logging.getLogger('flask').setLevel(level=logging.DEBUG)

	setup_vyper(parser, overrides)
	setup_logging()



def create_app():
	# falcon.API instances are callable WSGI apps
	app = Flask(__name__)
	bootstrap = Bootstrap(app)

	app.config.update(v.all_settings(uppercase_keys=True))

	from app.routes.auth import view
	app.register_blueprint(view)

	from app.routes.user import view
	app.register_blueprint(view)

	return app


def start():
	logger.info("Starting {}".format(settings.get("APP_NAME")))
	logger.info("Environment: {}".format(settings.get("ENV_NAME")))
	pass



