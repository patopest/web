import logging
import json
import requests
import datetime

from app.config import settings
from app.api import api

import functools
from .forms import RegisterForm, LoginForm
from flask import Blueprint, render_template, redirect, url_for, flash, session, g

view = Blueprint("auth", __name__)


@view.route("/", methods=["GET"])
def base():
	return render_template("index.html")


@view.route('/login', methods=['GET', 'POST'])
def login():
	if session.get('user_id') and session.get('token'):
		 return redirect(url_for('user.home', user_id = session['user_id']))

	form = LoginForm()
	if form.validate_on_submit():
		data = {
			"email": form.email.data,
			"password": form.password.data,
		}
		#r = requests.post(settings.get("API_URL")+"/login", data=json.dumps(data))
		r = api.login(body=data)

		if r.status_code == 202:
		#if True:
			return redirect(url_for('user.home', user_id = session['user_id']))
		else:
			flash((r.json().get('description') if not None else "Error whilst processing your request"))

	return render_template('auth.html', title='Sign In', form=form)


@view.route("/register", methods=["GET", "POST"])
def register():
	form = RegisterForm()
	if form.validate_on_submit():
		data = {
			"email": form.email.data,
			"password": form.password.data,
		}
		#r = requests.post(settings.get("API_URL")+"/register", data=json.dumps(data))
		r = api.register(body=data)

		if r.status_code == 201:
		#if True:
			return redirect(url_for('user.home', user_id = session['user_id']))
		else:
			flash((r.json().get('description') if not None else "Error whilst processing your request"))

	return render_template("auth.html", title='Register', form=form)


@view.route('/logout')
def logout():
	session.clear()
	return redirect(url_for('auth.base'))

#@view.before_app_request
def load_user():
	user_id = session.get('user_id')
	token = session.get('token')

	if user_id and token:
		return
	else:
		return redirect(url_for('auth.login'))


def login_required(view):
	@functools.wraps(view)
	def wrapped_view(**kwargs):
		if (session['user_id'] is None or session['token'] is None):
			session.clear()
			return redirect(url_for('auth.login'))
		if (session['last_login'] <= (datetime.datetime.utcnow() - datetime.timedelta(hours=24)).timestamp()):
			session.clear()
			return redirect(url_for('auth.login'))
			
		return view(**kwargs)

	return wrapped_view




