import logging
import json
import requests

from app.config import settings
from app.api import api
from .auth import login_required
from .forms import TaskForm, SettingsForm

from flask import Blueprint, render_template, redirect, url_for, flash, session

view = Blueprint("user", __name__, url_prefix='/user')

@view.route('/<string:user_id>', methods=['GET', 'POST'])
@login_required
def home(user_id):
	user = api.get('/users/' + user_id)
	if user.status_code == 403:
		return redirect(url_for('user.home', user_id=user_id))
	user = user.json()
	form = TaskForm()
	if form.validate_on_submit():
		body = {
			"course": {
				"dept": form.dept.data,
				"code": form.code.data,
				"crn": form.crn.data,
				"term": form.term.data
			},
			"email": user['email']
		}

		task_id = form.task_id.data
		if task_id == "new":
			r = api.post('/users/' + user_id + '/tasks', body=body)
		elif form.delete.data:
			r = api.delete('/users/' + user_id + '/tasks/' + task_id)
		else:
			r = api.put('/users/' + user_id + '/tasks/' + task_id, body=body)
		#print(r.json())
	#print("getting tasks")
	tasks = api.get('/users/' + user_id + '/tasks').json()
	#print(tasks)
	return render_template('home.html', title='Courses', user=user, tasks=tasks, form=form)


@view.route('/<string:user_id>/help', methods=['GET'])
@login_required
def help(user_id):
	user = {'id': user_id}
	return render_template('help.html', title="Help", user=user)


@view.route('/<string:user_id>/settings', methods=['GET', 'POST'])
@login_required
def settings(user_id):
	user = api.get('/users/' + user_id).json()
	form = SettingsForm()
	if form.validate_on_submit():
		body = {
			"email": user['email'],
			"password": form.password.data,
			"new_password": form.new_password.data
		}

		r = api.put('/users/' + user_id, body=body)

		if r.status_code == 200:
			flash('Successfully changed your password.')
		else:
			flash('Error whilst changing your password, please try again.')

	return render_template('settings.html', title="Settings", user=user, form=form)

@view.route('/<string:user_id>/settings/notifier/<string:user_email>', methods=['GET'])
@login_required
def notifier_test(user_id, user_email):
	body = {'email': user_email}
	r = api.post('/users/' + user_id + '/notifier', body=body)
	if r.status_code != 201:
		flash("There was an error processing your request. Please try again (later).")
	else:
		flash("Email notification created successfully!")
	
	return redirect(url_for('user.settings', user_id=user_id))
