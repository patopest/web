from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, HiddenField
from wtforms.validators import DataRequired, EqualTo, Email

class RegisterForm(FlaskForm):
	email = StringField('Email', validators=[DataRequired("Please enter an email address"), Email("Please enter a valid email address")])
	password = PasswordField('Password', validators=[DataRequired(), EqualTo('password2', "Passwords do not match")])
	password2 = PasswordField('Confirm Password', validators=[DataRequired(), EqualTo('password', "")])
	submit = SubmitField('Register')


class LoginForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired()], render_kw={"placeholder": "email@bambinito.co"})
    password = PasswordField('Password', validators=[DataRequired()], render_kw={"placeholder": "**********"})
    sremember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')


class TaskForm(FlaskForm):
	task_id = HiddenField('task_id')
	dept = StringField('Department', validators=[DataRequired()], render_kw={"placeholder": "COMP"})
	code = StringField('Code', validators=[DataRequired()], render_kw={"placeholder": "101"})
	crn = StringField('CRN', validators=[DataRequired()], render_kw={"placeholder": "98765"})
	term = StringField('Term', validators=[DataRequired()], render_kw={"placeholder": "Fall2034"})
	submit = SubmitField('Submit')
	delete = SubmitField('Delete')


class SettingsForm(FlaskForm):
	password = PasswordField('Current Password', validators=[DataRequired()])
	new_password = PasswordField(' New Password', validators=[DataRequired(), EqualTo('new_password2', "Passwords do not match")])
	new_password2 = PasswordField('Confirm Password', validators=[DataRequired(), EqualTo('new_password', "")])
	submit = SubmitField('Change Password')