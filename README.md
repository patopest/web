# Minerva App (frontend)
This is the repository for the frontend of the [Minerva App](https://app.minerva.bambinito.co) hacky project.  
Maintainer: ALM (bambinito.dev@gmail.com)  

The purpose of this api is to schedule tasks that will check available spaces at regular intervals for a McGill course.    
If a space is found, it will send you an email notification.   
This app significantly increases your chance of getting a space in the course you want...   

You can try it out at: https://app.minerva.bambinito.co   
The api used by the app is available at https://api.minerva.bambinito.co   
 
-> The repository for the api can also be found on [Gitlab](https://gitlab.com/patopest/api)


## Instructions:  

### To run Locally:   
- Clone the repo.  
```shell
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt
```
- In case you get a python thread crash:
```shell
echo 'export OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES' >> venv/bin/activate
```
  
- To run app:  
```shell
python run.py
```
(use --help to see all configuration arguments)   
App should now be available at http://localhost:3000/



### To run Dev:  
(app running in local Docker container)  
- Run api container beforehand with `--name api`

- Pull Docker image:
(https://gitlab.com/patopest/web/container_registry)
```shell
docker pull registry.gitlab.com/patopest/web
```

- OR build Docker image:
```shell
docker build -t web:1.0 .
```

- Finally run container:
```shell
docker run -it --detach --publish 3000:3000 --link api:api --name web -e APP_ENV_NAME="DEV" web:test
```
